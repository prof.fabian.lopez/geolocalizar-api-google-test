<!DOCTYPE html>
<html lang="en">
<head>
  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
    <title>Corroborar la utilización de API de Google</title>
     
    <style>
    body{
        font-family:arial;
        font-size:.8em;
    }
     
    input[type=text]{
        padding:0.5em;
        width:20em;
    }
     
    input[type=submit]{
        padding:0.4em;
    }
     
    #gmap_canvas{
        width:100%;
        height:30em;
    }
     
    #map-label,
    #address-examples{
        margin:1em 0;
    }
    </style>
 
</head>
<body>
 
<?php
if($_POST){
 
    // get latitude, longitude and formatted address
    $data_arr = geocode($_POST['address']);
 
    // if able to geocode the address
    if($data_arr){
         
        $latitude = $data_arr[0];
        $longitude = $data_arr[1];
        $formatted_address = $data_arr[2];
        
		echo $latitude ."<br>";
        echo $longitude ."<br>";
        echo $formatted_address ."<br>";
        	
    ?>
 
    <!-- google map will be shown here -->
    <div id="gmap_canvas">Cargando Mapa...</div>
    <div id='map-label'>Ubicando en el mapa la ubicación mas aprox.</div>
 
    <!-- JavaScript to show google map -->
    <script type="text/javascript" src="http://maps.google.com/maps/api/js"></script>    
    <script type="text/javascript">
        function init_map() {
            var myOptions = {
                zoom: 14,
                center: new google.maps.LatLng(<?php echo $latitude; ?>, <?php echo $longitude; ?>),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
            marker = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng(<?php echo $latitude; ?>, <?php echo $longitude; ?>)
            });
            infowindow = new google.maps.InfoWindow({
                content: "<?php echo $formatted_address; ?>"
            });
            google.maps.event.addListener(marker, "click", function () {
                infowindow.open(map, marker);
            });
            infowindow.open(map, marker);
        }
        google.maps.event.addDomListener(window, 'load', init_map);
    </script>
 
    <?php
 
    // if unable to geocode the address
    }else{
        echo "No map found.";
    }
}
?>
 
<div id='address-examples'>
    <div>Ejemplo de Dirección:</div>
    <div>1. Salta 150, Merlo, Buenos Aires</div>
    <div>2. 80 E.Rodriguez Jr. Ave. Libis Quezon City</div>
</div>
 
<!-- enter any address -->
<form action="" method="post">
    <input type='text' name='address' placeholder='Ingrese una dirección válida' />
    <input type='submit' value='Geocode!' />
</form>
 
<?php
 
// function to geocode address, it will return false if unable to geocode address
function geocode($address){
 
    // url encode the address
    $address = urlencode($address);
     // esta api de google deben renovarla!!!! key=AIzaSyAz1qO7tg5l_QKSZ8qYVK593DG_MossbQA
    // nueva AIzaSyBkarsCi90mHbxR0daoZdF0l-4HCzr-BEU

    // google map geocode api url
    $url = "https://maps.google.com/maps/api/geocode/json?address={$address}&key=AIzaSyDFdX_XAcK6ZZ7h8xUmw_RLcY6kXBiLf7s";
//    $url = "https://maps.google.com/maps/api/geocode/json?address={$address}&key=AIzaSyBkarsCi90mHbxR0daoZdF0l-4HCzr-BEU";
//    $url = "https://maps.google.com/maps/api/geocode/json?address={$address}&key=AIzaSyAz1qO7tg5l_QKSZ8qYVK593DG_MossbQA";
 
    // get the json response
 //   var_dump($url);

    $resp_json = file_get_contents($url);
 //   var_dump($resp_json); 
    // decode the json
    $resp = json_decode($resp_json, true);
 //   var_dump($resp);
 //   exit;
    // response status will be 'OK', if able to geocode given address 


    if($resp['status']=='OK'){
 
        // get the important data
        $lati = $resp['results'][0]['geometry']['location']['lat'];
        $longi = $resp['results'][0]['geometry']['location']['lng'];
        $formatted_address = $resp['results'][0]['formatted_address'];
         
        // verify if data is complete
        if($lati && $longi && $formatted_address){
         
            // put the data in the array
            $data_arr = array();            
             
            array_push(
                $data_arr, 
                    $lati, 
                    $longi, 
                    $formatted_address
                );
             
            return $data_arr;
             
        }else{
            return false;
        }
         
    }else{
		echo $resp['status'];
        return false;
    }
}
?>
 
</body>
</html>